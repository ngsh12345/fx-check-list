import React from 'react';

// Material-UI
import {
  Box,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper
} from '@mui/material';

// サマリデータの取得用関数
import { useTradeSummary } from '../hooks/trade_summary';

/**
 * 通貨ペアのタブ選択時の表示内容
 */
const CurrencyPairContent: React.FC = () => {
  const { tradeSummary, getBorderColor } = useTradeSummary();
  console.log(tradeSummary);
  return (
    <Box>
      {/* 通貨ペアの時間軸別にサマリを表示 */}
      {tradeSummary.map((elem) => {
        return (
          <Box key={elem.currencyPair}>
            <Typography
              variant="h6"
              gutterBottom
              margin={2}
              style={{
                borderBottom: 'solid 4px',
                borderBottomColor: getBorderColor(elem.currencyPair)
              }}
            >
              {elem.currencyPair}
            </Typography>
            <TableContainer component={Paper}>
              <Table sx={{}} size="small" aria-label="a dense table">
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: '55%' }}>項目名</TableCell>
                    <TableCell align="right">設定値</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {/* 現在のポジション */}
                  <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell
                      component="th"
                      scope="row"
                      sx={{ maxWidth: '10px' }}
                    >
                      現在のポジション
                    </TableCell>
                    <TableCell align="right">
                      {elem.followPosition === 'なし' &&
                      elem.reversePosition === 'なし'
                        ? 'なし'
                        : ''}
                      {elem.followPosition !== 'なし'
                        ? elem.followPosition
                        : ''}
                      {elem.reversePosition !== 'なし'
                        ? elem.reversePosition
                        : ''}
                    </TableCell>
                  </TableRow>
                  {/* 現在のトレンド方向 */}
                  <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell
                      component="th"
                      scope="row"
                      sx={{ maxWidth: '10px' }}
                    >
                      現在のトレンド方向
                    </TableCell>
                    <TableCell align="right">{elem.trend}</TableCell>
                  </TableRow>
                  {/* 前回のステータス(順張り) */}
                  <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell
                      component="th"
                      scope="row"
                      sx={{ maxWidth: '10px' }}
                    >
                      前回のステータス(順張り)
                    </TableCell>
                    <TableCell align="right">{elem.followPrevStatus}</TableCell>
                  </TableRow>
                  {/* 前回のステータス(逆張り) */}
                  <TableRow
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell
                      component="th"
                      scope="row"
                      sx={{ maxWidth: '10px' }}
                    >
                      前回のステータス(逆張り)
                    </TableCell>
                    <TableCell align="right">
                      {elem.reversePrevStatus}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        );
      })}
    </Box>
  );
};

export default CurrencyPairContent;
