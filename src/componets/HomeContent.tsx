import React from 'react';

// Material-UI
import {
  Box,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper
} from '@mui/material';

// Hook
import { useSimpleSummary } from '../hooks/simple_summary';

const HomeContent: React.FC = () => {
  // 簡易サマリ　Hook
  const { simpleSummary } = useSimpleSummary();
  console.log(simpleSummary);
  return (
    <Box>
      <Box>
        <Typography
          variant="h6"
          gutterBottom
          margin={2}
          style={{ borderBottom: 'solid 4px darkred' }}
        >
          監視足チェック中
        </Typography>
        <TableContainer component={Paper} style={{ marginBottom: '50px' }}>
          <Table sx={{}} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '55%' }}>通貨ペア</TableCell>
                <TableCell align="right">トレード方向</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {simpleSummary.monitoringData.map((elem) => (
                <TableRow
                  key={elem.currencyPair}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {elem.currencyPair}
                  </TableCell>
                  <TableCell align="right">{elem.tradeDirection}</TableCell>
                </TableRow>
              ))}
              {simpleSummary.monitoringData.length === 0 ? (
                <TableRow
                  key={-1}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {'なし'}
                  </TableCell>
                  <TableCell align="right">{'なし'}</TableCell>
                </TableRow>
              ) : (
                <></>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <Box>
        <Typography
          variant="h6"
          gutterBottom
          margin={2}
          style={{ borderBottom: 'solid 4px darkred' }}
        >
          中期足ブレイク待ち（2本目）
        </Typography>
        <TableContainer component={Paper} style={{ marginBottom: '50px' }}>
          <Table sx={{}} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '55%' }}>通貨ペア</TableCell>
                <TableCell align="right">トレード方向</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {simpleSummary.trendChangeingData.map((elem) => (
                <TableRow
                  key={elem.currencyPair}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {elem.currencyPair}
                  </TableCell>
                  <TableCell align="right">{elem.tradeDirection}</TableCell>
                </TableRow>
              ))}
              {simpleSummary.trendChangeingData.length === 0 ? (
                <TableRow
                  key={-1}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {'なし'}
                  </TableCell>
                  <TableCell align="right">{'なし'}</TableCell>
                </TableRow>
              ) : (
                <></>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <Box>
        <Typography
          variant="h6"
          gutterBottom
          margin={2}
          style={{ borderBottom: 'solid 4px darkred' }}
        >
          ポジション保有中
        </Typography>
        <TableContainer component={Paper} style={{ marginBottom: '50px' }}>
          <Table sx={{}} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '55%' }}>通貨ペア</TableCell>
                <TableCell align="right">トレード方向</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {simpleSummary.tradingData.map((elem) => (
                <TableRow
                  key={elem.currencyPair}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {elem.currencyPair}
                  </TableCell>
                  <TableCell align="right">{elem.tradeDirection}</TableCell>
                </TableRow>
              ))}
              {simpleSummary.tradingData.length === 0 ? (
                <TableRow
                  key={-1}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {'なし'}
                  </TableCell>
                  <TableCell align="right">{'なし'}</TableCell>
                </TableRow>
              ) : (
                <></>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <Box>
        <Typography
          variant="h6"
          gutterBottom
          margin={2}
          style={{ borderBottom: 'solid 4px darkred' }}
        >
          指値約定待ち
        </Typography>
        <TableContainer component={Paper}>
          <Table sx={{}} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '55%' }}>通貨ペア</TableCell>
                <TableCell align="right">トレード方向</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {simpleSummary.readyData.map((elem) => (
                <TableRow
                  key={elem.currencyPair}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {elem.currencyPair}
                  </TableCell>
                  <TableCell align="right">{elem.tradeDirection}</TableCell>
                </TableRow>
              ))}
              {simpleSummary.readyData.length === 0 ? (
                <TableRow
                  key={-1}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    sx={{ maxWidth: '10px' }}
                  >
                    {'なし'}
                  </TableCell>
                  <TableCell align="right">{'なし'}</TableCell>
                </TableRow>
              ) : (
                <></>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Box>
  );
};

export default HomeContent;
