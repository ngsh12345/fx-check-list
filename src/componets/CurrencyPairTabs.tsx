import React from 'react';

// Material-UI
import { Box, Tab, Tabs } from '@mui/material';

// Const
import { TAB_NAME_LIST } from '../const';

// Hook
import { useTab } from '../hooks/tab';

/**
 * 通貨ペアのタブ一覧
 * @returns
 */
const CurrencyPairTabs: React.FC = () => {
  /** タブ用　Hook */
  const { tabInfo, handleChangeTab } = useTab();

  return (
    <Box
      style={{ display: 'flex', justifyContent: 'center' }}
      sx={{ borderBottom: 1, borderColor: 'divider' }}
    >
      <Tabs
        value={tabInfo.index}
        onChange={handleChangeTab}
        aria-label="basic tabs example"
      >
        {TAB_NAME_LIST.map((tabName: string) => {
          return <Tab key={tabName} label={tabName} />;
        })}
      </Tabs>
    </Box>
  );
};

export default CurrencyPairTabs;
