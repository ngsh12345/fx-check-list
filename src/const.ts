export const CURRENCY_PAIR_LIST = ['USD/JPY', 'EUR/USD', 'GBP/USD'];
export const TAB_NAME_LIST = ['HOME'].concat(CURRENCY_PAIR_LIST);
