import React from 'react';

// Material-UI
import { Box } from '@mui/material';

// Component
import HomeContent from '../../componets/HomeContent';
import CurrencyPairContent from '../../componets/CurrencyPairContent';
import CurrencyPairTabs from '../../componets/CurrencyPairTabs';

// Hook
import { useTab } from '../../hooks/tab';

/**
 * ホーム画面
 * @returns
 */
const Home: React.FC = () => {
  // タブ用Hook
  const { tabInfo } = useTab();
  return (
    <Box>
      {/* 画面上部のタブ一覧 */}
      <CurrencyPairTabs />
      {/* タブ別の表示内容 */}
      {tabInfo.index === 0 ? (
        // 全体のサマリ画面
        <HomeContent />
      ) : (
        // 各通貨ペアのサマリ画面
        <CurrencyPairContent />
      )}
    </Box>
  );
};

export default Home;
