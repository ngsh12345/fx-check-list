import axios from 'axios';

// Const
import { CURRENCY_PAIR_LIST } from '../const';

/** Google Spread Sheet API URL */
const BASE_URL = `https://sheets.googleapis.com/v4/spreadsheets/${process.env.REACT_APP_GOOGLE_SPREAD_SHEET_ID}`;
/** APIキー（必須パラメータ） */
const QUERY_PARAMS = { key: process.env.REACT_APP_GOOGLE_API_KEY };

/** API取得後の成形したJSON型 */
export interface TradeSummary {
  /** 通貨ペア名 */
  currencyPair: string;
  /** 現在のトレンド方向 */
  trend: string;
  /** 現在の保有ポジション（順張り） */
  followPosition: string;
  /** 現在の保有ポジション（逆張り）*/
  reversePosition: string;
  /** 前回のステータス（順張り） */
  followPrevStatus: string;
  /** 前回のステータス（逆張り） */
  reversePrevStatus: string;
  /** ポジション名（順張り） */
  followName: string;
  /** ポジション名（逆張り） */
  reverseName: string;
}

//
/**
 * スプレッドシートのデータを取得する
 * @param successCallback 取得成功時のコールバック関数
 * @param failCallback 取得失敗時のコールバック関数
 */
export const getAllTradeSummary = async (
  /** 取得成功時のコールバック関数 */
  successCallback: Function,
  /** 取得失敗時のコールバック関数 */
  failCallback?: Function
) => {
  /** スプレッドシートの取得範囲セル */
  const range = 'A4:H14';

  try {
    // 同時実行するためにリクエスト関数を配列に格納
    const requests: any = [];
    CURRENCY_PAIR_LIST.forEach((elem) => {
      requests.push(
        axios.get(`${BASE_URL}/values/${elem.replace('/', '%2F')}!${range}`, {
          params: QUERY_PARAMS
        })
      );
    });

    // 全てのリクエストが完了するまで待機
    const response = editAllSummary(await Promise.all(requests));
    successCallback(response);
  } catch (error) {
    if (failCallback) {
      failCallback(error);
    }
  }
};

// 取得した全トレード状況のデータを編集する
/**
 *  取得データを編集する
 * @param res 取得結果の配列
 * @returns
 */
const editAllSummary = (res: any[]) => {
  const result: TradeSummary[] = [];

  /** 1時間足の列　要素番号 */
  const hour1Index = 0;
  /** 4時間足の列　要素番号 */
  const hour4Index = 3;
  /** 日足の列　要素番号 */
  const day1Index = 6;
  const lineIndexList = [hour1Index, hour4Index, day1Index];

  /** 通貨ペアの行　要素番号 */
  const currencyPairRowIndex = 0;
  /** トレンド方向の行　要素番号 */
  const trendRowIndex = 2;
  /** 順張り保有ポジションの行　要素番号 */
  const positionRowIndex_follow = 4;
  /** 逆張り保有ポジションの行　要素番号 */
  const positionRowIndex_reverse = 5;
  /** 順張り前回のステータスの行　要素番号 */
  const prevStatusRowIndex_follow = 8;
  /** 逆逆張り前回のステータスの行　要素番号 */
  const prevStatusRowIndex_reverse = 10;

  res.forEach((currencyPair: any) => {
    lineIndexList.forEach((lineIndex) => {
      const obj = {
        currencyPair: '',
        trend: '',
        followPosition: '',
        reversePosition: '',
        followPrevStatus: '',
        reversePrevStatus: '',
        followName: '',
        reverseName: ''
      };
      currencyPair.data.values.forEach((rowList: any[], rowIndex: number) => {
        switch (rowIndex) {
          case currencyPairRowIndex:
            obj.currencyPair = rowList[lineIndex];
            break;
          case trendRowIndex:
            obj.trend = rowList[lineIndex];
            if (obj.trend === '上昇トレンド') {
              obj.followName = '順張り';
              obj.reverseName = '逆張り';
            } else {
              obj.followName = '順張り';
              obj.reverseName = '逆張り';
            }

            break;

          case positionRowIndex_follow:
            obj.followPosition = rowList[lineIndex].includes('なし')
              ? 'なし'
              : `順張り`;
            break;

          case positionRowIndex_reverse:
            obj.reversePosition = rowList[lineIndex].includes('なし')
              ? 'なし'
              : '逆張り';
            break;

          case prevStatusRowIndex_follow:
            obj.followPrevStatus = rowList[lineIndex];
            break;

          case prevStatusRowIndex_reverse:
            obj.reversePrevStatus = rowList[lineIndex];
            break;

          default:
            break;
        }
      });
      result.push(obj);
    });
  });

  return result;
};
