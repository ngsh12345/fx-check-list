import React from 'react';
import { RecoilRoot } from 'recoil';

// CSS
import './App.css';

// Component
import Home from './pages/Home/Home';

const App: React.FC = () => (
  <RecoilRoot>
    <Home />
  </RecoilRoot>
);

export default App;
