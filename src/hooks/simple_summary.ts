import { useEffect } from 'react';
import { useRecoilState } from 'recoil';

// State
import { SimpleSummary, simpleSummaryState } from '../states/trade';

// Api
import { TradeSummary, getAllTradeSummary } from '../apis/spread_sheet_api';

/**
 * ホーム画面の簡易的なサマリ用　Hook
 * @returns
 */
export const useSimpleSummary = () => {
  const [simpleSummary, setSimpleSummary] = useRecoilState(simpleSummaryState);

  useEffect(() => {
    getAllTradeSummary((response: TradeSummary[]) => {
      // 各データに振り分け

      setSimpleSummary({
        monitoringData: editMonitoringData(response),
        trendChangeingData: editTrendChangeingData(response),
        tradingData: editTradingData(response),
        readyData: editReadyData(response)
      });
    });
  }, [setSimpleSummary]);

  return { simpleSummary };
};

/**
 * 監視中の通貨ペアのデータ生成
 * @param dataList 取得したデータ
 * @returns
 */
const editMonitoringData = (dataList: TradeSummary[]) => {
  const result: SimpleSummary[] = [];
  dataList.forEach((elem) => {
    const obj = {
      currencyPair: '',
      tradeDirection: ''
    };
    if (elem.followPrevStatus.includes('監視足アラート')) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection = elem.followName;

      if (elem.followPrevStatus.includes('再エントリー')) {
        obj.tradeDirection += '（再エントリー待ち）';
      }
    }
    if (elem.reversePrevStatus.includes('監視足アラート')) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection += elem.reverseName;

      if (elem.reversePrevStatus.includes('再エントリー')) {
        obj.tradeDirection += '（再エントリー待ち）';
      }
    }
    if (obj.currencyPair) {
      result.push(obj);
    }
  });

  return result;
};

/**
 * 順張りで中期足転換中の通貨ペアのデータ生成
 * @param dataList 取得したデータ
 * @returns
 */
const editTrendChangeingData = (dataList: TradeSummary[]) => {
  const result: SimpleSummary[] = [];
  dataList.forEach((elem) => {
    const obj = {
      currencyPair: '',
      tradeDirection: ''
    };
    const flag1 =
      elem.followPrevStatus.includes('中期足アラート') &&
      elem.followPrevStatus.includes('2本目');
    const flag2 =
      elem.followPrevStatus.includes('エントリーレジサポ') &&
      elem.followPrevStatus.includes('2本目');
    if (flag1 || flag2) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection = elem.followName;
    }
    if (obj.currencyPair) {
      result.push(obj);
    }
  });

  return result;
};

/**
 * トレード中の通貨ペアのデータ生成
 * @param dataList 取得したデータ
 * @returns
 */
const editTradingData = (dataList: TradeSummary[]) => {
  const result: SimpleSummary[] = [];
  dataList.forEach((elem) => {
    const obj = {
      currencyPair: '',
      tradeDirection: ''
    };
    if (!elem.followPosition.includes('なし')) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection = elem.followName;
    }
    if (!elem.reversePosition.includes('なし')) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection += elem.reverseName;
    }
    if (obj.currencyPair) {
      result.push(obj);
    }
  });

  return result;
};

/**
 * 指値約定待ちの通貨ペアのデータ生成
 * @param dataList 取得したデータ
 * @returns
 */
const editReadyData = (dataList: TradeSummary[]) => {
  const result: SimpleSummary[] = [];
  dataList.forEach((elem) => {
    const obj = {
      currencyPair: '',
      tradeDirection: ''
    };
    if (elem.followPrevStatus.includes('指値約定待ち')) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection = elem.followName;
    }
    if (elem.reversePrevStatus.includes('指値約定待ち')) {
      obj.currencyPair = elem.currencyPair;
      obj.tradeDirection += elem.reverseName;
    }
    if (obj.currencyPair) {
      result.push(obj);
    }
  });

  return result;
};
