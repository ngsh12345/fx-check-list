import React, { useCallback } from 'react';
import { useRecoilState } from 'recoil';

// Const
import { TAB_NAME_LIST } from '../const';

// State
import { tabInfoState } from '../states/tab';

/**
 * タブ用　Hook
 * @returns
 */
export const useTab = () => {
  /** 選択されたタブ */
  const [tabInfo, setTabInfo] = useRecoilState(tabInfoState);

  /** タブ選択時の切り替え処理 */
  const handleChangeTab = useCallback(
    (event: React.SyntheticEvent, newValue: number) => {
      setTabInfo({
        index: newValue,
        name: TAB_NAME_LIST[newValue]
      });
    },
    [setTabInfo]
  );

  return { tabInfo, handleChangeTab };
};
