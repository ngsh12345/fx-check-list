import { useEffect } from 'react';
import { useRecoilState } from 'recoil';

// State
import { tradeSummaryState } from '../states/trade';
import { tabInfoState } from '../states/tab';

// Api
import { TradeSummary, getAllTradeSummary } from '../apis/spread_sheet_api';

/**
 * トレードサマリ用　Hook
 * @returns
 */
export const useTradeSummary = () => {
  const [tradeSummary, setTradeSummary] = useRecoilState(tradeSummaryState);
  const [tabInfo] = useRecoilState(tabInfoState);

  useEffect(() => {
    // データ取得後、選択した通貨ペアのデータのみを抽出
    getAllTradeSummary((response: TradeSummary[]) => {
      setTradeSummary(
        response.filter((elem) => {
          return elem.currencyPair.includes(tabInfo.name);
        })
      );
    });
  }, [setTradeSummary, tabInfo.name]);

  /**
   * BorderBottomColorで使用する色を取得
   * @param currencyPair 通貨ペア名
   * @returns 下線の色名
   */
  const getBorderColor = (currencyPair: string): string => {
    if (currencyPair.includes('1時間足')) {
      return 'blue';
    }
    if (currencyPair.includes('4時間足')) {
      return 'lightgreen';
    }
    return 'yellow';
  };

  return { tradeSummary, getBorderColor };
};
