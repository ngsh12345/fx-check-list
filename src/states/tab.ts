import { atom } from 'recoil';

// Const
import { TAB_NAME_LIST } from '../const';

interface TabInfo {
  index: number;
  name: string;
}

/**
 * 全トレード履歴
 */
export const tabInfoState = atom<TabInfo>({
  key: 'tabInfoState',
  default: {
    index: 0,
    name: TAB_NAME_LIST[0]
  }
});
