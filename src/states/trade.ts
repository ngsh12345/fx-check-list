import { atom } from 'recoil';

// Api
import { TradeSummary } from '../apis/spread_sheet_api';

/**
 * 全トレード履歴
 */
export const tradeSummaryState = atom<TradeSummary[]>({
  key: 'tradeSummaryState',
  default: []
});

/** ホーム画面で使用する簡易的な表示用 */
export type SimpleSummary = {
  currencyPair: string; // 通貨ペア名
  tradeDirection: string; // トレード方向
};

// 各データの集合
export type AllSimpleSummary = {
  monitoringData: SimpleSummary[]; // 監視中の通貨ペア
  trendChangeingData: SimpleSummary[]; // 順張りで中期足転換中の通貨ペア
  tradingData: SimpleSummary[]; // トレード中の通貨ペア
  readyData: SimpleSummary[]; // 指値約定待ちの通貨ペア
};

/**
 * ホーム画面の簡易的なサマリ
 */
export const simpleSummaryState = atom<AllSimpleSummary>({
  key: 'simpleSummaryState',
  default: {
    monitoringData: [], // 監視中の通貨ペア
    trendChangeingData: [], // 順張りで中期足転換中の通貨ペア
    tradingData: [], // トレード中の通貨ペア
    readyData: [] // 指値約定待ちの通貨ペア
  }
});
